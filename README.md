## Clone

```bash
git clone https://gitlab.com/FanchG/ldap-migrate-dn.git
```

## Config

- Open Script:
```bash
emacs migrate_dn.sh
```

- Edit variable (or not):
```
ldap_host=192.168.103.20
ou_people="ou=people,dc=ifb,dc=local"
filter="(!(ou:dn:=services))"
```

## Run

```
chmod +x migrate_dn.sh
./migrate_dn.sh
```

## Check Generated Content

```
emacs migrate_dn.ldif
```

## Update ldap (or not)

```
ldapmodify -h $ldap_host -cx -w <password> -D "cn=admin,dc=ifb,dc=local" -f migrate_dn.ldif
```
