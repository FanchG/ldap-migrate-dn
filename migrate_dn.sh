### Rename all dn to 'uid=' + username + ',' + suffix

ldap_host=192.168.103.20
ou_people="ou=people,dc=ifb,dc=local"
#filter="(!(ou:dn:=services))"
filter=""

gen_migrate_dn_ldif () {

    ldapsearch -h "$ldap_host" -xLLL -b "$ou_people" "$filter" dn | sort -u | while read line
    do
        dn=$(echo -n "$line" | cut -f2- -d ' ')

        # check if dn is in base64
        echo $line | grep '::' > /dev/null
        if [ $? -eq 0 ]
        then
            dn=$(echo -n "$dn" | base64 -d)
        fi

        if [ "$dn" != "" -a "$dn" != "$ou_people" ]
        then

            uid=$(ldapsearch -h "$ldap_host" -xLLL -s base -b "$dn" uid | grep 'uid:')
            # if there is no uid (should not happen)
            if [ $? -ne 0 ]
            then
                echo "ERROR for $dn" >> /dev/stderr
                continue
            fi

            # cn=$(ldapsearch -h "$ldap_host" -xLLL -s base -b "$dn" cn | grep 'cn:' )

            # check if cn is in base64
            # echo $cn | grep '::' > /dev/null
            # if [ $? -eq 0 ]
            # then
            #    cn="cn: "$(echo -n "$cn" | cut -f2- -d ' ' | base64 -d)
            # fi

            echo "dn: $dn"
            echo "changetype: modrdn"
            echo "newrdn: "$(echo "$uid"| sed -e s/': '/'='/g)
            echo "deleteoldrdn: 0" # keep old rdn as entry ie cn attribute
            # echo "newsuperior: "$(echo "$cn"| sed -e s/': '/'='/g)",$ou_people"
            echo ""
        fi
    done
}

gen_migrate_dn_ldif > migrate_dn.ldif
