

ldif_dir=./ldif

ldap_host='192.168.1.16'
ldap_login='cn=admin,dc=sb-roscoff,dc=fr'
ldap_pass='my'
group_ou='ou=groups,dc=sb-roscoff,dc=fr'
project_ou='ou=projects,ou=groups,dc=sb-roscoff,dc=fr'

mkdir -p $ldif_dir

for i in /shared/projects/*
do
    prj=$(basename $i)
    olddn='cn='$prj','$group_ou
    newdn='cn='$prj','$project_ou

    num_result=$(num show-ldap --cn $prj)
    found_old_in_ldap=$(echo "$num_result" | grep "$olddn" | wc -l)
    found_new_in_ldap=$(echo "$num_result" | grep "$newdn" | wc -l)

    if [ 1 -eq "$found_old_in_ldap" -a 0 -eq "$found_new_in_ldap" ]
       then
           ldif_file=$ldif_dir/$prj.ldif

           echo "Generating $ldif_file"

           # https://ldapwiki.com/wiki/Modrdn

           echo "dn: $olddn" > $ldif_file
           echo "changetype: modrdn" >> $ldif_file
           echo "newrdn: cn=$prj" >> $ldif_file
           # we want to move it
           echo "deleteoldrdn: 1" >> $ldif_file
           echo "newsuperior: $project_ou" >> $ldif_file

           echo "" >> $ldif_file

           ldapmodify -h "$ldap_host" -cx -w "$ldap_pass" -D "$ldap_login" -f $ldif_file

           ## my sync should do the job after this
           mongo gomngr --quiet --eval 'db.getCollection("groups").remove({"name": "'$prj'"})'
    elif [ 0 -ne "$found_new_in_ldap" ]
    then
        echo "Project $prj already a project in ldap"
    else
        echo "Project $prj not found as groups in ldap"
        echo "$num_result"
    fi
done


systemctl start my.sync


for prj in /shared/projects/*
do
    name=$(basename $prj)

    # todo add a clean way to find the current disk quota
	  #	mongo gomngr --quiet --eval 'db.getCollection("projects").updateOne({ id: "'$name'" }, {$set: {size: '$quotagb'}})'

	  mongo gomngr --quiet --eval 'db.getCollection("projects").updateOne({ id: "'$name'" }, {$set: {path: "'$prj'"}})'

done
